export const DashBoardLayout = ({ children }) => {
    return (
        <div>
            <section>
                side bar
            </section>
            <main>
                {children}
            </main>
        </div>
    )
}