import ReactDOM from "react-dom"
import { Switch, BrowserRouter as Router } from "react-router-dom"

// Import Routes all
import { authRoutes, userRoutes } from "./routers/allRoutes"

// Import all middleware
import Authmiddleware from "./routers/middleware/Authmiddleware"

// Import scss
import "./assets/scss/theme.scss"
import NonAuthLayout from "./components/NonAuthLayout"
import { AppProvider } from "./store/appStore"
import { AuthProvider } from "./store/authStore"
import { DashBoardLayout } from "./layouts/dashboardLayout"

const App = () => {
    return (
        <AuthProvider>
            <AppProvider>
                <Router>
                    <Switch>
                        {authRoutes.map((route, idx) => (
                            <Authmiddleware
                                path={route.path}
                                layout={NonAuthLayout}
                                component={route.component}
                                key={idx}
                                isAuthProtected={false}
                            />
                        ))}
                        {userRoutes.map((route, idx) => (
                            <Authmiddleware
                                path={route.path}
                                layout={DashBoardLayout}
                                component={route.component}
                                key={idx}
                                isAuthProtected={true}
                                exact
                            />
                        ))}
                    </Switch>
                </Router>
            </AppProvider>
        </AuthProvider>
    )
}

ReactDOM.render(<App />, document.getElementById("root"))