export const CONST_LOGIN = {
    TYPES_LOGIN: [
        {
            rol: 3,
            url: "CCYT",
            name: "CCYT",
            color: "#00c2ec"
        },
        {
            rol: 1,
            url: "UGEL",
            name: "UGEL",
            color: "#0dbc3e"
        },
        {
            rol: 2,
            url: "DRE-GRE",
            name: "DRE/GRE",
            color: "#e5e939"
        },
        {
            rol: 4,
            url: "CONCYTEC",
            name: "CONCYTEC",
            color: "#ec7100"
        },
    ]
}