import React from "react"
import PropTypes from "prop-types"
import { Route, Redirect } from "react-router-dom"
import { useAuth } from "../../store/authStore"

const Authmiddleware = ({
  component: Component,
  layout: Layout,
  isAuthProtected,
  ...rest
}) => {
  const { authUser } = useAuth("state")
  return (
    <Route
      {...rest}
      render={props => {
        if (isAuthProtected && Object.keys(authUser).length === 0) {
          return (
            <Redirect
              to={{ pathname: "/login", state: { from: props.location } }}
            />
          )
        }
        if (!isAuthProtected && Object.keys(authUser).length) {
          return (
            <Redirect
              to={{ pathname: "/", state: { from: props.location } }}
            />
          )
        }
        return (
          <Layout>
            <Component {...props} />
          </Layout>
        )
      }}
    />
  )
}
Authmiddleware.propTypes = {
  isAuthProtected: PropTypes.bool,
  component: PropTypes.any,
  location: PropTypes.object,
  layout: PropTypes.any,
}

export default Authmiddleware
