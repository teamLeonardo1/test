import { Redirect } from "react-router"
import { Login } from "../pages/Authentication/Login"
import { SelectLogin } from "../pages/Authentication/SelectLogin"

// const login = lazy(() => import("../pages/Authentication/Login"))

const userRoutes = [
  { path: "/pago", component: ()=> <div>pago</div> },
  { path: "/dashboard", component: ()=> <div>dashboard</div> },
  { path: "/", exact: true, component: () => <Redirect to="/dashboard" /> },
]

const authRoutes = [
  // { path: "/logout", component: Logout },
  // { path: "/login", component: Login },
  // { path: "/forgot-password", component: ForgetPwd },
  // { path: "/register", component: Register },
  { path: "/login/:select", component: Login }, 
  { path: "/login", component: SelectLogin }, 
  // { path: "/", exact: true, component: () => <Redirect to="/login" /> },
]

export { userRoutes, authRoutes }
