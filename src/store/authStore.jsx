import { authService } from '../helpers/services/auth.service';

import { createContext, useContext, useState } from "react";
import { getLocalStorage, setLocalStorage } from '../helpers/utils/localStorage';

const CTXAuth = createContext({})

export function AuthProvider({ children }) {
    //state login
    const [isValidate, setIsValidate] = useState(false)
    const [isErroValidate, setIsErrorValidate] = useState(false)
    const [loadingValidate, setLoadingValidate] = useState(false)
    const [textError, setTextError] = useState("")
    const [authUser, setAuthUser] = useState(getLocalStorage("authUser", {}))
    //state register
    const loginState = async (payloadLogin, type) => {
        setLoadingValidate(true)
        const data = await authService.login(payloadLogin)
        const { payload, token } = data
        if (payload && Object.keys(payload).length && token) {
            const rol = payload.rol
            if (rol !== type.rol) {
                setIsErrorValidate(true)
                setIsValidate(false)
                setAuthUser({})
                setLocalStorage("authUser", {})
                setTextError("El usuario no es del tipo " + type.name)
                setLoadingValidate(false)
                throw new Error("no coinciden")
            }
            setAuthUser({ ...payload, token })
            setLocalStorage("authUser", { ...payload, token })
            setIsErrorValidate(false)
            setIsValidate(true)
        }
        // if (!Object.keys(data).every(key => validateVariant.indexOf(key) > -1)) {
        //     setIsErrorValidate(false)
        //     setAuthUser(data)
        //     setTextError("El usuario no es del timpo " + type)
        //     setLoadingValidate(false)
        //     return;
        // }
        setLoadingValidate(false)
    }



    return <CTXAuth.Provider
        value={{
            state: {
                isValidate,
                isErroValidate,
                textError,
                loadingValidate,
                authUser
            },
            dispatch: {
                loginState,
            }
        }}
    >
        {children}
    </CTXAuth.Provider>
}

export const useAuth = (KEY) => { const contexto = useContext(CTXAuth); return contexto[KEY] }

export default CTXAuth