import { createContext, useState } from "react";

const CTXApp = createContext({})



export function AppProvider({ children }) {

    const [state,] = useState({});

    return <CTXApp.Provider
        value={{
            state: state
        }}
    >
        {children}
    </CTXApp.Provider>
}
export default CTXApp