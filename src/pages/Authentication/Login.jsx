import MetaTags from 'react-meta-tags';

import { Row, Col, CardBody, Card, Alert, Container, Toast, ToastHeader, ToastBody } from "reactstrap"
import { Link, useParams, useHistory } from "react-router-dom"

// availity-reactstrap-validation
import { AvForm, AvField } from "availity-reactstrap-validation"


// import images
import logoSm from "../../assets/images/logo-sm.png";
import { useAuth } from '../../store/authStore';
import { CONST_LOGIN } from '../../constants/login.const';

export const Login = props => {
  //hoock
  const { loginState } = useAuth("dispatch")
  const { select } = useParams()
  const objSelect = CONST_LOGIN.TYPES_LOGIN.find(tl => tl.url === select)
  const { go } = useHistory()
  // handleValidSubmit
  const handleValidSubmit = async (event, values) => {
    // props.loginUser(values, props.history)
    try {
      await loginState(values, objSelect)
      go(0)
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <>
      <MetaTags>
        <title>Login || {objSelect.name}</title>
      </MetaTags>
      <div className="home-btn d-none d-sm-block">
        <Link to="/" className="text-dark">
          <i className="fas fa-home h2" />
        </Link>
      </div>
      <div className="account-pages my-5 pt-sm-5">
        <Container>
          <Row className="justify-content-center">
            <Col md={8} lg={6} xl={4}>
              <Card className="overflow-hidden">
                <div
                  // style={""+ `background-color: rgb(13, 188, 62);`}
                  style={{ backgroundColor: objSelect.color }}
                >
                  <div
                    className="text-primary text-center p-4"
                  >
                    <h5 className={`font-size-20 ${["DRE-GRE", "CCYT"].indexOf(objSelect.url) > -1 ? "text-secondary" : "text-white"}`}>
                      Welcome Back ! {objSelect.name}
                    </h5>
                    <p className={`${["DRE-GRE", "CCYT"].indexOf(objSelect.url) > -1 ? "text-muted" : "text-white-50"}`}>
                      Sign in to continue to {objSelect.name}.
                    </p>
                    <Link to="/" className="logo logo-admin">
                      <img src={logoSm} height="24" alt="logo" />
                    </Link>
                  </div>
                </div>

                <CardBody className="p-4">
                  <div className="p-3">
                    <AvForm
                      className="form-horizontal mt-4"
                      onValidSubmit={(e, v) => {
                        handleValidSubmit(e, v)
                      }}
                    >
                      {props.error && typeof props.error === "string" ? (
                        <Alert color="danger">{props.error}</Alert>
                      ) : null}

                      <div className="mb-3">
                        <AvField
                          name="email"
                          label="Email"
                          value="moises.loartec@gmail.com"
                          className="form-control"
                          placeholder="Enter email"
                          type="email"
                          required
                        />
                      </div>

                      <div className="mb-3">
                        <AvField
                          name="password"
                          label="Password"
                          value="123123"
                          type="password"
                          required
                          placeholder="Enter Password"
                        />
                      </div>

                      <Row className="mb-3">
                        <Col sm={6}>
                          <div className="form-check">
                            <input type="checkbox" className="form-check-input" id="customControlInline" />
                            <label className="form-check-label" htmlFor="customControlInline">Remember me</label>
                          </div>
                        </Col>
                        <Col sm={6} className="text-end">
                          <button
                            className="btn btn-primary w-md waves-effect waves-light"
                            type="submit"
                          >
                            Log In
                          </button>
                        </Col>
                      </Row>
                      <Row className="mt-2 mb-0 row">
                        <div className="col-12 mt-4">
                          <Link to="/forgot-password">
                            <i className="mdi mdi-lock"></i> Forgot your
                            password?
                          </Link>
                        </div>
                      </Row>
                      <Toast>
                        <ToastHeader>
                          Reactstrap
                        </ToastHeader>
                        <ToastBody>
                          This is a toast on a white background — check it out!
                        </ToastBody>
                      </Toast>

                    </AvForm>
                  </div>
                </CardBody>
              </Card>
              <div className="mt-5 text-center">
                <p>
                  Don&#39;t have an account ?{" "}
                  <Link
                    to="register"
                    className="fw-medium text-primary"
                  >
                    {" "}
                    Signup now{" "}
                  </Link>{" "}
                </p>
                <p>
                  © {new Date().getFullYear()} Veltrix. Crafted with{" "}
                  <i className="mdi mdi-heart text-danger" /> by Themesbrand
                </p>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  )
}
