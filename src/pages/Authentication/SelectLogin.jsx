import { MetaTags } from "react-meta-tags"
import { useHistory } from "react-router"
import { CONST_LOGIN } from "../../constants/login.const"

export const SelectLogin = () => {

    const { push } = useHistory()

    const handelChange = (values) => {
        push("/login/" + values)
    }

    return <>
        <MetaTags>
            <title>Login || select</title>
        </MetaTags>
        <div
            className="select-home"
        >
            <div className="select-home-title">
                Bienvenido/a
            </div>
            <div className="select-home-description">
                Al sistema Integrado de Gestión de clubes de ciencia y tecnología (SIGECCYT), que tiene como finalidad pública brindar información detallada a los usuarios responsables de la implementación de los clubes a nivel nacional.
            </div>
            <br />
            <div className="card-select-container">
                {
                    CONST_LOGIN.TYPES_LOGIN.map((_, idx) => {
                        return <div
                            key={idx}
                            onClick={() => handelChange(_.url)}
                            className={"card-select-" + _.url}
                            style={{ backgroundColor: _.color }}
                        >
                            <div className="card-select-img"></div>
                            <div className="card-select-title">
                                {_.name}
                            </div>
                        </div>
                    })
                }
            </div>
        </div>
    </>
}